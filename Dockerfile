FROM python:3.7-alpine
MAINTAINER uaskhat
COPY src/requirements.txt /app/src/
RUN pip install -r /app/src/requirements.txt
ADD . /app
WORKDIR /app
ENTRYPOINT ["python"]
CMD ["src/app.py"]
