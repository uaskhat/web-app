import unittest
import app


class TestApp(unittest.TestCase):

    def add(self):
        app.app_add('!test')

    def dell(self):
        app.app_del('!test')

    def test_get(self):
        app.app_add('!test')
        self.assertEqual(app.app_get(0), '!test')
        app.app_del('!test')

if __name__ == '__main__':
    unittest.main()