from flask import Flask, flash, redirect, render_template, request, session, abort
import os
import redis
from healthcheck import HealthCheck
import mockredis
import socket

app = Flask(__name__)

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    result = sock.connect_ex(('redis',6379))
except socket.error:
    result = 1


if result == 0:
    redisClient = redis.StrictRedis(host='redis', port=6379, db=0)
    redis_local = False
else:
    redisClient = mockredis.mock_strict_redis_client()
    redis_local = True
sock.close()

"""
if not ping:
    redisClient = mockredis.mock_strict_redis_client()
    redis_local = True
else:
"""


health = HealthCheck(app, "/health")


def redis_available():
    client = redisClient
    info = client
    return True, "redis ok"


health.add_check(redis_available)


def app_add(username):
    redisClient.lpush('UserList', username)


def app_del(username):
    redisClient.lrem('UserList', 1, username)


def app_get(user_index):
    username = redisClient.lindex('UserList', user_index)
    if username:
        return username.decode("utf-8")


@app.route('/')
def home():
    if redis_local:
        redis_status = "local"
    else:
        redis_status = ""
    user_list = ''
    user = app_get(0)
    for i in range(5):
        if app_get(i):
            user_list += app_get(i)+" "

    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return render_template('main.html', user=user, luser=user_list, redis_status=redis_status)

 
@app.route('/login', methods=['POST'])
def do_admin_login():
 
    post_name = str(request.form['username'])
    app_add(post_name)
    session['logged_in'] = True
    return home()

 
@app.route("/logout")
def logout():
    session['logged_in'] = False
    return home()

 
if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(host='0.0.0.0', port=4000)
    # app.run(debug=True,host='0.0.0.0', port=4000)
