# WebApp(flask)

To run app locally, you need:
- install docker, docker-compose
- create /app folder and setup user privileges
- clone repository
- run 'docker-compose build && docker-compose up' in /app directory  
