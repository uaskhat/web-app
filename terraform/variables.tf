variable "public_key_path" {
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.
Ensure this keypair is added to your local SSH agent so provisioners can
connect.
Example: ~/.ssh/terraform.pub
DESCRIPTION
  default = "/home/aura/.ssh/id_rsa.pub"
}

  variable "aws_vpc" {
    description = "aws_vpc"
    default = ""
  }

variable "key_name" {
  description = "Desired name of AWS key pair"
  default     = "id_rsa"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "eu-west-3"
}

# Ubuntu Precise 12.04 LTS (x64)
variable "aws_amis" {
  default = {
    eu-west-1 = "ami-674cbc1e"
    us-east-1 = "ami-1d4e7a66"
    us-west-1 = "ami-969ab1f6"
    us-west-2 = "ami-8803e0f0"
    eu-west-3 = "ami-0451ae4fd8dd178f7"
  }

}